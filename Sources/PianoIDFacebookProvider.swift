import UIKit

import PianoOAuth

import FBSDKCoreKit
import FBSDKLoginKit

public class PianoIDFacebookProvider: PianoIDSocialProvider {
    
    public override var name: String { "facebook" }
    
    public override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) {
        FBSDKLoginKit.ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    public override func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any?) -> Bool {
        return FBSDKLoginKit.ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    public override func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return FBSDKLoginKit.ApplicationDelegate.shared.application(application, open: url, options: options)
    }
    
    public override func signIn(controller: UIViewController, completion: @escaping (Bool, String?, Error?) -> Void) {
        DispatchQueue.main.async {
            FBSDKLoginKit.LoginManager().logIn(
                permissions: ["public_profile", "email"],
                from: controller
            ) { result, error in
                if let e = error {
                    completion(false, nil, e)
                    return
                }
                
                if let r = result {
                    if r.isCancelled {
                        completion(true, nil, nil)
                        return
                    }
                    
                    if let t = r.token?.tokenString {
                        completion(false, t, nil)
                        return
                    }
                }
                
                completion(false, nil, PianoIDError.facebookSignInFailed)
            }
        }
    }
}
