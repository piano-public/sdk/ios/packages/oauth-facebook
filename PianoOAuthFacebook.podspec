Pod::Spec.new do |s|
  s.name         = 'PianoOAuthFacebook'
  s.version      = '2.8.6'
  s.swift_version = '5.10'
  s.summary      = 'Enables iOS apps to sign in with Piano.io and Facebook'
  s.homepage     = 'https://gitlab.com/piano-public/sdk/ios/packages/oauth-facebook'
  s.license      = { :type => 'Apache License, Version 2.0', :file => 'LICENSE' }
  s.author       = 'Piano Inc.'
  s.platform     = :ios
  s.ios.deployment_target = '12.0'
  s.source       = { :git => 'https://gitlab.com/piano-public/sdk/ios/packages/oauth-facebook.git', :tag => "#{s.version}" }
  s.resource_bundle = {
      "PianoSDK_OAuthFacebook" => ["Sources/Resources/*"]
  }
  s.source_files = 'Sources/*.swift', 'Sources/**/*.h'
  s.static_framework = true
  s.dependency 'PianoOAuth', "~> #{s.version}"
  s.dependency 'FBSDKLoginKit', '~> 17.4.0'
end
