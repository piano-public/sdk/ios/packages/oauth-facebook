// swift-tools-version:5.10

import PackageDescription

let package = Package(
    name: "PianoOAuthFacebook",
    platforms: [
        .iOS(.v12)
    ],
    products: [
        .library(
            name: "PianoOAuthFacebook",
            targets: ["PianoOAuthFacebook"]
        )
    ],
    dependencies: [
        .package(url: "https://github.com/facebook/facebook-ios-sdk", .upToNextMinor(from: "17.4.0")),
        .package(url: "https://gitlab.com/piano-public/sdk/ios/package", .upToNextMinor(from: "2.8.6"))
    ],
    targets: [
        .target(
            name: "PianoOAuthFacebook",
            dependencies: [
                .product(name: "FacebookLogin", package: "facebook-ios-sdk"),
                .product(name: "PianoOAuth", package: "package")
            ],
            path: "Sources",
            resources: [
                .copy("Resources/PrivacyInfo.xcprivacy")
            ]
        ),
        .testTarget(
            name: "PianoOAuthFacebookTests",
            dependencies: ["PianoOAuthFacebook"],
            path: "Tests"
        )
    ]
)
