# Piano OAuth integration with Facebook for iOS

[![Version](https://img.shields.io/cocoapods/v/PianoOAuthFacebook.svg?style=flat)](http://cocoapods.org/pods/PianoOAuthFacebook)
[![Platform](https://img.shields.io/cocoapods/p/PianoOAuthFacebook.svg?style=flat)](http://cocoapods.org/pods/PianoOAuthFacebook)
[![License](https://img.shields.io/cocoapods/l/PianoOAuthFacebook.svg?style=flat)](http://cocoapods.org/pods/PianoOAuthFacebook)

## Requirements
- iOS 16.0+
- Xcode 15.3
- Swift 5.10 (iOS target version 12+)

## Installation

### [CocoaPods](https://cocoapods.org/)

Add the following lines to your `Podfile`.

```
use_frameworks!

pod 'PianoOAuthFacebook', '~> 2.8.6'
```

Then run `pod install`. For details of the installation and usage of CocoaPods, visit [official web site](https://cocoapods.org/).

### [Swift Package Manager](https://developer.apple.com/documentation/swift_packages/adding_package_dependencies_to_your_app)
Add the component `PianoOAuthFacebook` from the repository:

**URL:** https://gitlab.com/piano-public/sdk/ios/packages/oauth-facebook

**Version:** 2.8.6

## PianoOAuthFacebook Usage

### Imports
```swift
import PianoOAuthFacebook
```

### Configuration
You should configure your application as described here: https://developers.facebook.com/docs/swift/register-your-app#configuresettings

### Integration
Add before the `PianoIDApplicationDelegate.shared.application` function call
```swift
// iOS 14+
@main
struct MyApp: App {
    init() {
        PianoID.shared.add(socialProvider: PianoIDFacebookProvider())
    }
}

// iOS 13 and earlier versions
class AppDelegate: UIResponder, UIApplicationDelegate {
    override init() {
        super.init()
        PianoID.shared.add(socialProvider: PianoIDFacebookProvider())
    }
    ...
}
```
